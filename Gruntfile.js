module.exports = function(grunt) {
	
	grunt.initConfig({
		jade : {
			options : {
				pretty : true
			},
			files: {
				expand: true,
				cwd: 'src/view/',
				src: ['*.jade'],
				dest: 'dist/',
				ext: '.html'
			}
		},
		sass : {
			dist: {
				options: {
					style: 'expanded',
					update : true,
					noCache: true, 
					sourcemap: 'none'
				},
				files: {
					'dist/assets/css/template.css': 'src/template/template.scss'
				}
			}
		},
		combine_mq : {
			new_filename: {
				options: {
					beautify: true
				},
				src: 'dist/assets/css/template.css',
				dest: 'dist/assets/css/template.css'
			}
		},
		stripCssComments: {
			dist: {
				files: {
					'dist/assets/css/template.css': 'dist/assets/css/template.css'
				}
			}
		},
		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1
			},
			target: {
				files: {
					'dist/assets/css/template.css' : 'dist/assets/css/template.css'
				}
			}
		},
		uglify: {
			my_target: {
				files: {
					'dist/assets/js/main.min.js': ['dist/assets/js/main.js']
				}
			}
		},
		watch : {
			html : {
				files : ['src/view/*.jade'],
				tasks : ['jade'],
				options : {
					spawn : false
				}
			},
			css : {
				files : ['src/template/**/*.scss'],
				tasks : ['sass'],
				options : {
					spawn : false
				}
			},
			js : {
				files : ['dist/assets/js/main.js'],
				tasks : ['uglify']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-jade');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-combine-mq');
	grunt.loadNpmTasks('grunt-strip-css-comments');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	grunt.registerTask('default', ['combine_mq', 'stripCssComments', 'cssmin', 'uglify']);

}